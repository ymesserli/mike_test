'use strict';

describe('Service: wizards', function () {

  // load the service's module
  beforeEach(module('mikeTestApp'));

  // instantiate service
  var wizards;
  beforeEach(inject(function (_wizards_) {
    wizards = _wizards_;
  }));

  it('should do something', function () {
    expect(!!wizards).toBe(true);
  });

});
