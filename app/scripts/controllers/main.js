'use strict';

angular.module('mikeTestApp')
  .controller('MainCtrl', function ($scope, Wizards) {
    
    $scope.currentLecture = Wizards.lecture ?  Wizards.lecture.name : null;

    $scope.lectures = [
    	{
    		name: "Lectures 1"
    	},
    	{
    		name: "Lectures 2"
    	},
    	{
    		name: "Lectures 3"
    	}
    ] ;
     $scope.questions = [
    	{
    		name: "Question 1"
    	},
    	{
    		name: "Questions 2"
    	},
    	{
    		name: "Questions 3"
    	}
    ] ;

    $scope.onclickLecture = function(lecture){

  
    	Wizards.lecture = lecture;
    	console.log(Wizards.lecture);
    }

 


  });
