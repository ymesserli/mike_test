'use strict';

angular.module('mikeTestApp', [])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/step1.html',
        controller: 'MainCtrl'
      })
      .when('/step2', {
        templateUrl: 'views/step2.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
